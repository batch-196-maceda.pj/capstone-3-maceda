import React from "react";
import {Link} from 'react-router-dom'
import{Row, Col, Button,} from 'react-bootstrap';


export default function Error(){
	return(

		<Row>
			<Col className="p-5">
				<h1>404 ERROR:Page Cannot Be Found</h1>
				<p>
				The page you are trying to access Cannot Be Found!.

				Please check the URL if it has been coppied properly. 
				</p>
			<Button variant="primary" as={Link} to="/" >Return to Home Page</Button>
			</Col>

		</Row>

		)
};
