import {useState, useEffect,useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import{Navigate,useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Register(){

	const{user}=useContext(UserContext);
	const history = useNavigate();

	// state hooks to store the values of the input fields
	const [firstName,setFirstName]=useState('');
	const [lastName,setLastName]=useState('');
	const [mobileNo, setMobileNo]=useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	
	// state to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	

	function registerUser(e){
		 // prevents page redirection via form submission
		 e.preventDefault();

		 fetch('http://localhost:4000/users/checkEmailExists',{
		 	method:'POST',
		 	headers:{
		 		'Content-Type':'application/json'
		 	},
		 	body: JSON.stringify({
		 		email: email
		 	})
		 })
		 .then(res => res.json())
		 .then(data=>{
		 	console.log(data);

		 	if(data){
		 		Swal.fire({
		 			title: "Duplicate email found",
		 			icon:"info",
		 			text:"the email taht you're trying to register already exist"
		 		});
		 	}else{
		
		 		fetch('http://localhost:4000/register',{
		 			method:'POST',
		 			headers:{
		 				'Content-Type':'application/json'
		 			},
		 			body: JSON.stringify({
		 				firstName:firstName,
		 				lastName: lastName,
		 				email:email,
		 				mobileNo:mobileNo,
		 				password:password
		 			})
		 		})
		 		.then(res => res.json())
		 		.then(data =>{
		 			console.log(data);

		 			if(data.email){
		 				Swal.fire({
		 					title:'Registration successful!',
		 					icon:'success',
		 					text:'Thank you for Registering!'
		 				})

		 				history('/login');
		 			}else{
		 				Swal.fire({
		 					title:'Registration Failed!',
		 					icon:'error',
		 					text:'Something went wrong, try again!'
		 				})
		 			}
		 		})


		 	};
		 })

		 // clear input fields
		 setFirstName('');
		 setLastName('');
		 setMobileNo('');
		 setEmail('');
		 setPassword('');
		

		
	};


	// Syntax:
		// useEffect(() => {}, [])	
	useEffect(() => {
		// validation to enable the submit button when all fields are populated and both passwords match
		if((email !== '' && password !== '' && firstName !== "" && lastName!== "" && mobileNo!=="" && mobileNo.length === 11)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [firstName,lastName,mobileNo,email, password]);

	return(
		(user.id !== null)?
			<Navigate to="/products"/>
		:
		<>
		<h1>Register Here:</h1>
		<Form onSubmit={e => registerUser(e)}>

		<Form.Group controlId="firstName">
			<Form.Label>First Name:</Form.Label>
			<Form.Control
				type="text"
				placeholder="Enter your first name here"
				required
				value= {firstName}
				onChange= {e => setFirstName(e.target.value)}
			/>
			
		</Form.Group>
		<Form.Group controlId="lastName">
			<Form.Label>Last Name:</Form.Label>
			<Form.Control
				type="text"
				placeholder="Enter your family name here"
				required
				value= {lastName}
				onChange= {e => setLastName(e.target.value)}
			/>
			
		</Form.Group>'<Form.Group controlId="mobileNumber">
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your mobile number here"
					required
					value= {mobileNo}
					onChange= {e => setMobileNo(e.target.value)}
				/>
				
			</Form.Group>'
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here!"
					required
					value= {email}
					onChange= {e => setEmail(e.target.value)}
				/>
				
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here!"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

		

		{ isActive ?			
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
				Register
			</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>
				Register
			</Button>
		}

		</Form>
		</>

	)
}
