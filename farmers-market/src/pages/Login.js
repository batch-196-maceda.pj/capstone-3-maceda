import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login(){

	const {user,setUser} = useContext(UserContext);
	console.log(user);


	const [email, setEmail] = useState('');
	const [loginPass, setloginPass] = useState('');
	const [isActive, setIsActive] = useState(false);

	function loginUser(e){
		 // prevents page redirection via form submission
		 e.preventDefault();
		 //Syntax: fetch(url,option)
		 fetch('http://localhost:4000/users/login',{
		 	method: 'POST',
		 	headers:{
		 		'Content-Type':'application/json'
		 	},
		 	body: JSON.stringify({
		 		email:email,
		 		password:loginPass
		 	})
		 })
		 .then(res=> res.json())
		 .then(data =>{
		 	console.log(data)

		 	if(typeof data.accessToken !== "undefined"){
		 		localStorage.setItem('token',data.accessToken)
		 		retrieveUserDetails(data.accessToken);

		 		Swal.fire({
		 			title:"Login Successful",
		 			icon:"success",
		 			text:"Welcome to Farmers Market!"
		 		})
		 	}else{
		 		Swal.fire({
		 			title:"Authentication Failed!",
		 			icon:"error",
		 			text:"Check your Credentials."
		 		})
		 	}
		 })

		 setEmail('');
		 setloginPass('');

		
	};
const retrieveUserDetails =(token)=>{

	fetch('http://localhost:4000/users/details',{
		headers:{
			Authorization: `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);


		setUser({
			id:data._id,
			isAdmin: data.isAdmin
		})
	})
}


	useEffect(() => {
		// validation to enable the submit button when all fields are populated and both passwords match
		if(email !== '' && loginPass !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, loginPass]);
return(
		(user.id !== null)?
			<Navigate to="/products"/>
		:
		<>
		<h1>Login Here:</h1>
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group controlId="loginEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value= {email}
					onChange= {e => setEmail(e.target.value)}
				/>
				
			</Form.Group>

			<Form.Group controlId="loginPass">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value={loginPass}
					onChange={e => setloginPass(e.target.value)}
				/>
			</Form.Group>

			<p>Not Yet Registered?<Link to ="/register">Register here!</Link></p>
		{ isActive ?			
		<Button className="mt-3 mb-5" variant="success" type="submit" id="loginBtn">
		Login 
		</Button>
		:
		<Button className="mt-3 mb-5" variant="danger" type="submit" id="loginBtn" disabled>
		Login 
		</Button>
		}
		</Form>
		</>

)
}