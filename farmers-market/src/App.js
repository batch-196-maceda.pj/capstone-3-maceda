import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import{useState,useEffect} from 'react';
import{UserProvider} from './UserContext';
import ProductView from './components/ProductView';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/Error';
import './App.css';

function App() {
  const[user, setUser]= useState({
        id: null,
        isAdmin: null
    });

    const unsetUser = () =>{
        localStorage.clear();
    };
    useEffect(()=>{
      
       

     fetch('http://localhost:4000/users/getUserDetails',{
        headers:{
            Authorization:`Bearer ${localStorage.getItem('token')}`
        }
     })
     .then (res=> res.json())   
     .then (data=>{
        console.log(data);

        if (typeof data._id !== "undefined"){
            setUser({
                id:data._id,
                isAdmin: data.isAdmin
            })
        }else{
            setUser({
                id:null,
                isAdmin:null
            })
        }
     })
    },[])

  return(
//react fragments used for two self closing elements in react. 

    <>
    <UserProvider value={{user,setUser,unsetUser}}>
    <Router>
        <AppNavbar/>
        <Container>
            <Routes>
                <Route exact path="/" element={<Home/>}/>
                <Route exact path="/products" element={<Products/>}/>
                <Route exact path="/ProductView/:productId" element={<ProductView/>}/>
                <Route exact path="/register" element={<Register/>}/>
                <Route exact path="/login" element={<Login/>}/>
                <Route exact path="/logout" element={<Logout/>}/>
                <Route exact path="*" element={<PageNotFound/>}/>
           </Routes>  
         </Container>
    </Router>  
    </UserProvider> 
    </>
    )

  
}

export default App;