import React from 'react';

//ccreates a context object
// a data type of an object that stores information that can shared with other components within the app. 

const UserContext = React.createContext();


//provider component allows the other components to use the context object and supply them the necessary information

export const UserProvider  = UserContext.Provider;


//we export to utilize the context created. 
export default UserContext;