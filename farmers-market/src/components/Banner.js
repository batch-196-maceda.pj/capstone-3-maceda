import{Row, Col, Button} from 'react-bootstrap';


export default function Banner(){
	return(

		<Row>
			<Col className="p-5">
				<h1>Farmers Market</h1>
				<p>Freshly picked Quality Fruits and Vegies</p>
				<Button variant="primary">Shop now!</Button>
			</Col>

		</Row>

		)
};