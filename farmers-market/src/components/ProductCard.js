import{useState} from 'react';
import{Row,Col,Card,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}){
	
const{name,description,price,_id} =productProp

	return(
	<Row>
			<Col xs={12} md={12}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						{name}
					</Card.Title>
					<Card.Subtitle>
						Description:
					</Card.Subtitle>
						<Card.Text>
						{description}
						</Card.Text>
					
					<Card.Subtitle>
					Price: 
					</Card.Subtitle>
					<Card.Text>
					{price}
					</Card.Text>

					<Link className="btn btn-primary" to={`/productView/${_id}`}>View Product details</Link>
					</Card.Body>
			</Card>

			</Col>
	</Row>

		)
};