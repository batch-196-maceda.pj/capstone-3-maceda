import{Container, Row, Col, Card, Button} from 'react-bootstrap';
import{useState,useEffect,useContext} from 'react';
import{useParams, useNavigate,Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function ProductView(){
	const{user}=useContext(UserContext);
	const history = useNavigate();
	const {productId} = useParams();


	const[name,setName]=useState("");
	const [description, setDescription] = useState("");
	const[price,setPrice]= useState(0);

	const purchase = (productId) =>{
		fetch('http://localhost:4000/products',{
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);


		if(data){
			Swal.fire({
				title:'Successfully Purchased!',
				icon:'success',
				text:'Thank you for Purchasing!'
			});

			history("/products");
		}else{
			Swal.fire({
				title:'Something went wrong!',
				icon:'error',
				text: 'Please try again later'
			})
		}
		})
	};

	useEffect(()=>{
		console.log(productId)
		fetch(`http://localhost:4000//viewUsers/:productId/${productId}`)
		.then(res => res.json())
		.then(data=>{
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	},[productId]);

	return(
		<Container className ="mt-5">
			<Row>
				<Col lg={{span:6, offset: 3}}>
					<Card>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>Php {price}</Card.Text>
						
						{user.id !== null?
							<Button variant="primary" onClick={()=>purchase(productId)}>Buy now!</Button>
							:
							<Link className="btn btn-danger" to="/login">Log In</Link>
						}

					</Card>
				</Col>
			</Row>

		</Container>

		)
}