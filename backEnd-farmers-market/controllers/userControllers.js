// import User model
const User = require("../models/User");
// import bcrypt
const bcrypt = require("bcrypt");

// bcrypt is a package which allows us to has our passwords to add a layer of security for our users' details
const auth = require("../auth");

module.exports.viewAll = (req,res)=>{
    User.find({}, "_id email mobileNo").then(result => res.send(result)).catch(error => res.send(error))
}

// register new user
module.exports.register = (req,res) => {
	// checks if email is already used with an existing account. If already in use, reject registration. Otherwise, add new user.
	User.find({email:req.body.email})
	.then(result => {
		if(result.length === 0){
			const hashedPw = bcrypt.hashSync(req.body.password,10)
    
			let newUser = new User({
		
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				email: req.body.email,
				password: hashedPw,
				mobileNo: req.body.mobileNo
		
			})
			newUser.save().then(result => res.send(result)).catch(error => res.send("Oh no! Something went wrong :("))
		} else {
			res.send("Email already in use! Please use another one.")
		}
	}
	).catch(error => res.send(error))

}

// login new user
module.exports.login = (req,res) => {
	User.findOne({email:req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			res.send({message: "No User Found."})
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})

			} else {
				return res.send({message: "Incorrect Password"})
			}

		}

	}).catch(() => "Oh no. Something went wrong :(")
}

// view user details
module.exports.viewUser = (req,res) => {
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// make a user admin
module.exports.setAdmin = (req,res) => {
	let update = {
		isAdmin: true
	}
	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send("Something went wrong. Try again."))
}

// make remove user as admin
module.exports.removeAdmin = (req,res) => {
	let update = {
		isAdmin: false
	}
	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send("Something went wrong. Try again."))
}
