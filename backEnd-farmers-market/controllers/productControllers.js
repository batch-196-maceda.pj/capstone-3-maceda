// import Product model
const Product = require("../models/Product");
const Order = require("../models/Order");
const User = require("../models/User")

const auth = require("../auth");

// retrieve all products
module.exports.getAllProducts = (req,res)=>{
    // finds all products but only showing name, description, and price
	Product.find({isActive:true}, "name description price numberOfOrders")
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

//sort products by number of order
module.exports.sortByOrder = (req,res)=>{
	sort = {numberOfOrders: -1}
	Product.find({isActive:true}, "name description price numberOfOrders").sort(sort)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// view users who ordered product
module.exports.viewUser = (req,res) =>{
	Order.find({})
	.then(async result =>{
		users = []
		for(n=0; n<result.length; n++){
			for(i=0; i<result[n].products.length; i++){
				if(result[n].products[i].productId === req.params.productId){
					if(users.indexOf(result[n].userId) === -1){
						users.push(result[n].userId)
					}
				}
			}
		}
		userObj = []
		for(i=0; i<users.length; i++){
			let user = await User.findById(users[i], "email mobileNo -_id").then(result=>{
				return result
			}).catch(error => error)
			userObj.push(user)
		}
		res.send(userObj)
	})
	.catch(error => res.send(error))
}

// retrieve a single product
module.exports.getSingleProduct = (req,res)=>{
	Product.findOne({_id:req.params.productId,isActive:true}, "-__v -isActive")
	.then(result => {
        if(result){res.send(result)}
        else {res.send("Couldn't retrieve product :(. Either it doesn't exist or it was archived.")}})
	.catch(error => res.send(error))
};

// create a product
module.exports.createProduct = (req,res)=>{
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newProduct.save().then(result => res.send(result)).catch(error => res.send("Invalid input!"))
};

// update a product
module.exports.updateProduct = (req,res)=>{
	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
        updatedOn: new Date()
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => {
		if(result === null){
			res.send("Product not found!")
		} else{
			res.send(result)
		}
	})
	.catch(error => res.send(error))
}

//archive product
module.exports.archiveProduct = (req,res)=>{
    let update = {
        isActive: false
    }
    Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => {
		if(result === null){
			res.send("Product not found!")
		} else{
			res.send(result)
		}
	})
	.catch(error => res.send(error))
}

// activate product
module.exports.activateProduct = (req,res)=>{
    let update = {
        isActive: true
    }
    Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => {
		if(result === null){
			res.send("Product not found!")
		} else{
			res.send(result)
		}
	})
	.catch(error => res.send(error))
}
