const express = require("express");
const mongoose =require("mongoose");
const app = express();
const port = process.env.PORT || 4000;
const cors = require('cors');

mongoose.connect("mongodb+srv://admin:admin123@cluster0.il0ermo.mongodb.net/Capstone3APi?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let db=mongoose.connection;

db.on('error',console.error.bind(console, "MongoDB Connection Error."))

db.once('open',()=> console.log("Connected to MongoDB."))

app.use(express.json());

app.use(cors());


const userRoutes = require("./Routes/userRoutes");

app.use("/users",userRoutes);

const productRoutes = require("./Routes/productRoutes");

app.use("/products",productRoutes);

const orderRoutes = require("./Routes/orderRoutes");

app.use("/order",orderRoutes);

app.listen(port,()=>console.log(`Server is running at port ${port}`));