const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");

const { verify } = auth;

const { verifyAdmin } = auth;

//get all products
router.get("/", productControllers.getAllProducts);

//get all products sorted by number of orderss
router.get("/sortByOrder", productControllers.sortByOrder);

// get single product
router.get("/:productId", productControllers.getSingleProduct);

// get single product
router.get("/viewUsers/:productId", verify, verifyAdmin, productControllers.viewUser);

// create product
router.post("/createProduct", verify, verifyAdmin, productControllers.createProduct);

// update product fields
router.put("/updateProduct/:productId", verify, verifyAdmin, productControllers.updateProduct);

// archive product
router.put("/archiveProduct/:productId", verify, verifyAdmin, productControllers.archiveProduct);

// activate product
router.put("/activateProduct/:productId", verify, verifyAdmin, productControllers.activateProduct);

module.exports = router;