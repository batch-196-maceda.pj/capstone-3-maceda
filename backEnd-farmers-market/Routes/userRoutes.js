const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const { verify } = auth;

const { verifyAdmin } = auth;

const { verifyFullControl } = auth;

//view all users, admin only
router.get("/", verify, verifyAdmin, userControllers.viewAll);

//register
router.post("/register",userControllers.register);

// login
router.post("/login", userControllers.login);

// get user details
router.get("/getUserDetails", verify, userControllers.viewUser);

// set as admin
router.put("/setAdmin/:userId", verify, verifyAdmin, userControllers.setAdmin);

// remove as admin
//router.put("/removeAdmin/:userId", verify, verifyFullControl, userControllers.removeAdmin);

module.exports = router;